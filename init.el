;; package management
(package-initialize)
(require 'package)
(add-to-list 'package-archives '("MELPA" . "http://melpa.milkbox.net/packages/"))

;; stuff below is deprecated but installs use-package if not there yet
;; (unless (package-installed-p 'use-package)
;;   (package-refresh-contents)
;;   (package-install 'use-package))


;; appearance -------------------------------------------------------
(use-package zenburn-theme ; worlds best color theme
  :ensure t
  :config
  (load-theme 'zenburn t))

(set-frame-font "Inconsolata 11")
(when (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(when (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(toggle-frame-maximized)

;; other ------------------------------------------------------------
(setq visible-bell t) ; do not ding!
(setq inhibit-splash-screen t) ; disable start screen
(setq backup-directory-alist `(("." . ,(concat user-emacs-directory "backups")))) ; backup directory

;; editing -----------------------------------------------------------
(set-default 'truncate-lines t) ; don't wrap lines ...
(setq truncate-partial-width-windows nil) ; ... not even in split windows
(line-number-mode t) ; show current line
(column-number-mode t) ; show current column
(show-paren-mode t) ; highlight those parentheses
(setq-default indent-tabs-mode nil) ; spaces are better than tabs
(setq require-final-newline t) ; all files should end with newline
(add-hook 'before-save-hook 'delete-trailing-whitespace) ; nobody needs no trailing spaces

;; ivy ---------------------------------------------------------------
(use-package ivy
  :ensure t)

(setq enable-recursive-minibuffers t)
(setq ivy-use-virtual-buffers t)

(global-set-key (kbd "C-x b") 'ivy-switch-buffer)

(global-set-key "\C-s" 'swiper)

(global-set-key (kbd "M-x") 'counsel-M-x)
(global-set-key (kbd "M-y") 'counsel-yank-pop)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)


;; coding-------------------------------------------------------------
;; -------------------------------------------------------------------

;; syntax check
(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

;; language server protocal for emacs
(setq gc-cons-threshold 100000000) ;; 100mb
(setq read-process-output-max (* 1024 1024)) ;; 1mb

(use-package lsp-mode
  :ensure t
  :hook (
         (python-mode . lsp)
         )
  :commands lsp)

(setq lsp-flycheck-live-reporting t)

;; fancy ide user interface
(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode)

(setq lsp-ui-doc-enable t
      lsp-ui-doc-include-signature t
      lsp-ui-doc-position 'top
      lsp-ui-peek-enable t)

;; code completion
(use-package company-lsp
  :ensure t
  :commands company-lsp)

(setq company-minimum-prefix-length 1
      company-idle-delay 0.0)

;; (setq company-lsp-async t
;;       company-lsp-cache-candidates nil)


;; todo -------------------------------------------------------------
;; ------------------------------------------------------------------
;; which-key
;; gitter


;; custom stuff -----------------------------------------------------
;; ------------------------------------------------------------------
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)
